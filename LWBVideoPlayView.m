//
//  LWBVideoPlayView.m
//  ceshias
//
//  Created by apple on 17/6/9.
//  Copyright © 2017年 LWB. All rights reserved.
//

#import "LWBVideoPlayView.h"
#import <MobileVLCKit/MobileVLCKit.h>
#import <math.h>
#import <MediaPlayer/MediaPlayer.h>

@interface LWBVideoPlayView ()<VLCMediaPlayerDelegate,VLCMediaThumbnailerDelegate>
@property(nonatomic,strong)VLCMediaPlayer *player;
@property(nonatomic,strong)UIView *playerView;
@property(nonatomic,strong)NSURL *videoURL;
//开始播放时标记(判断是否第一次开始播放)
@property(nonatomic,assign)BOOL startFlag;
@property(nonatomic,strong)MPVolumeView * volumeView;
//竖屏状态的frame
@property(nonatomic,assign)CGRect frameVerticalView;
//拖动在左起始位置
@property(nonatomic,assign)BOOL panLeftFlog;
//拖动在右起始位置
@property(nonatomic,assign)BOOL panRightFlog;
//工具栏释放收起标记
@property(nonatomic,assign)BOOL  isToolbarHidden;
@property(nonatomic,strong)UIView *toolbarView;
//播放或者暂停
@property(nonatomic,assign)BOOL isPlay;
//进度条
@property(nonatomic,strong)UISlider *toolSlider;
@property(nonatomic,strong) UISlider* volumeViewSlider;
//播放按钮
@property(nonatomic,strong)UIButton *butPlay;
//全屏按钮
@property(nonatomic,strong)UIButton *butFullscreen;
@property(nonatomic,assign)BOOL isFullscreenFlog;
@property(nonatomic,strong)UIView *gesturesView;
@property(nonatomic,strong)NSTimer *timer;
//手势左右滑动标记
@property(nonatomic,assign) BOOL gesturesFlogX;
//手势上下滑动标记
@property(nonatomic,assign) BOOL gesturesFlogY;
//手势开始滑动时坐标
@property(nonatomic,assign)CGRect gesturesStartCoordinates;
@property(nonatomic,assign)float currentVolume;
@property(nonatomic,assign)float currentBrightness;
@property(nonatomic,assign)float currentProgress;

@end
@implementation LWBVideoPlayView


-(instancetype)initWithFrame:(CGRect)frame{
    if (self =[super initWithFrame:frame]) {
        [self initViodeView:frame];
        [self initToolbarView:frame];
        [self registeredNotice];
        [self setTapGestureRecognizer];
        [self setUpPanGestureRecognizer];
        //开始播放标记
        _startFlag =YES;
        //是否屏幕选择代理通知
        _isScreensState =NO;
        self.frameVerticalView =frame;
        _isGiveupToolbar =NO;
        _isToolbarHidden =YES;
        _isFullscreenFlog =NO;
//        _verticalCrossScreenFlog =YES;
        //定时关闭工具栏
        [self timingShutdownToolbar];
    }
    return self;
}
-(void)initToolbarView:(CGRect)frame{
    _gesturesView =[[UIView alloc]initWithFrame:frame];
    _gesturesView.alpha=1;
//    _gesturesView.backgroundColor =[UIColor whiteColor];
    [self addSubview:_gesturesView];
    CGFloat toolHeight =frame.size.height/6;
    CGFloat toolWidth = self.frame.size.width;
    _toolbarView = [[UIView alloc]init];
    _toolbarView.frame=CGRectMake(0, self.frame.size.height-toolHeight, toolWidth, toolHeight);
    _toolbarView.backgroundColor =[UIColor blackColor];
    _toolbarView.alpha =0.7;
    [self addSubview:_toolbarView];
    _toolSlider = [[UISlider alloc]init];
    _toolSlider.frame=CGRectMake(50, 5,toolWidth-102, 20);
    _toolSlider.value =0;
    _toolSlider.minimumValue =0;
    _toolSlider. continuous =YES;
    [_toolSlider addTarget:self action:@selector(sliderAction) forControlEvents:UIControlEventValueChanged];
    [_toolSlider setThumbImage:[UIImage imageNamed:@"Player Control Nob"] forState:UIControlStateNormal];
    [_toolbarView addSubview:_toolSlider];
    _butPlay = [[UIButton alloc]initWithFrame:CGRectMake(15, _toolSlider.frame.origin.y+_toolSlider.frame.size.height+2,20, 15)];
    [_butPlay setImage:[UIImage imageNamed:@"Pause Icon"] forState:UIControlStateNormal];
    [_butPlay addTarget:self action:@selector(butPlayAction) forControlEvents:UIControlEventTouchUpInside];
    [_toolbarView addSubview:_butPlay];
    _butFullscreen = [[UIButton alloc]initWithFrame:CGRectMake(_toolbarView.frame.size.width-40, _butPlay.frame.origin.y,20, 15)];
     [_butFullscreen setImage:[UIImage imageNamed:@"Full Screen Icon"] forState:UIControlStateNormal];
     [_butFullscreen addTarget:self action:@selector(butFullscreenAction) forControlEvents:UIControlEventTouchUpInside];
    [_toolbarView addSubview:_butFullscreen];
}
//全屏按钮点击方法
-(void)butFullscreenAction{
    if (_isFullscreenFlog) {
         [self mandatoryVerticalScreen];
    }else{
        [self mandatoryLandscape];
    }
}

//开始播放按钮点击方法
-(void)butPlayAction{
    if (_isPlay) {
        [self videoPause];
    }else{
        [self videoPlay];

    }
}
//slider执行方法
-(void)sliderAction{
    [self setUpCurrentProgress:_toolSlider.value/_toolSlider.maximumValue*1.0];
}
//定时器执行收起工具栏
-(void)timingShutdownToolbar{
    _timer =[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(shutdownToolbar) userInfo:nil repeats:NO];
}
-(void)shutdownToolbar{
    if (_isToolbarHidden) {
        [self hiddenToolbar];
    }
}
//显示工具栏
-(void)showToolbar{
    [UIView animateWithDuration:1 animations:^{
        self.toolbarView.transform = CGAffineTransformMakeTranslation(0, 0);
        self.toolbarView.alpha =1;

    }];
    _isToolbarHidden = YES;
    _timer =[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(shutdownToolbar) userInfo:nil repeats:NO];
}
//隐藏工具栏
-(void)hiddenToolbar{
    [UIView animateWithDuration:1 animations:^{
         self.toolbarView.transform = CGAffineTransformMakeTranslation(0,self.toolbarView.frame.size.height);
        self.toolbarView.alpha =0;
    }];
     _isToolbarHidden =NO;
    [_timer invalidate];
}

//注册横竖屏通知
-(void)registeredNotice{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doRotateAction:) name:UIDeviceOrientationDidChangeNotification object:nil];
}
//注册横竖屏监听后调用的方法
-(void)doRotateAction:(NSNotification *) notification{
    if (_isScreensState) {
            if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationPortrait) {
                [self setVerticalScreenToolBarFrame];
            }else{
                [self setLandscapeToolbarFrame];
            }
        
        if ([self.delegate respondsToSelector:@selector(SomehowTheScreenSwitch)]) {
             [self.delegate SomehowTheScreenSwitch];
        }else{
            NSLog(@"代理没实现，SomehowTheScreenSwitch:");
        }

    }
}
//竖屏
-(void)setVerticalScreenToolBarFrame{
     [_butFullscreen setImage:[UIImage imageNamed:@"Full Screen Icon"] forState:UIControlStateNormal];
    _isFullscreenFlog =NO;
    self.frame =self.frameVerticalView;
    self.gesturesView.frame =self.frame;
    self.playerView.frame =self.frame;
    CGFloat toolHeight =self.frame.size.height/6;
    CGFloat toolWidth = self.frame.size.width;
    self.toolbarView.frame=CGRectMake(0, self.frame.size.height-toolHeight, toolWidth, toolHeight);
    _toolSlider.frame=CGRectMake(50, 5,toolWidth-102, 20);
    _butPlay.frame=CGRectMake(15, _toolSlider.frame.origin.y+_toolSlider.frame.size.height+2,20, 15);
    _butFullscreen.frame =CGRectMake(_toolbarView.frame.size.width-40, _butPlay.frame.origin.y,20, 15);
}
-(void)mandatoryLandscape{
     [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
}
//设置横屏
-(void)setLandscapeToolbarFrame{
    [_butFullscreen setImage:[UIImage imageNamed:@"Min. Icon"] forState:UIControlStateNormal];
    //全屏标记设置为yes
    _isFullscreenFlog =YES;
    self.frame=self.superview.frame;
    self.gesturesView.frame =self.frame;
    self.playerView.frame =self.frame;
    CGFloat toolHeight =self.frame.size.height/6;
    CGFloat toolWidth = self.frame.size.width;
    self.toolbarView.frame=CGRectMake(0, self.frame.size.height-toolHeight, toolWidth, toolHeight);
    _toolSlider.frame=CGRectMake(80, 5,toolWidth-160, 20);
    _butPlay.frame=CGRectMake(35, _toolSlider.frame.origin.y+_toolSlider.frame.size.height+2,20, 15);
    _butFullscreen.frame =CGRectMake(_toolbarView.frame.size.width-60, _butPlay.frame.origin.y,20, 15);
    NSLog(@">>>landscape");

}
-(void)mandatoryVerticalScreen{
    [self interfaceOrientation:UIInterfaceOrientationPortrait];
}
//强制横竖屏方法
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector             = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val                  = orientation;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}

-(void)initViodeView:(CGRect)frame{
    _playerView=[[UIView alloc]initWithFrame:frame];
    _player = [[VLCMediaPlayer alloc] initWithOptions:nil];
    _player.drawable =_playerView;
    self.player.delegate =self;
    self.playVideos = [NSMutableArray new];
    [self setUpVolumeViewSlider];
    _player.audio.volume =1;
    [self addSubview:_playerView];
}
//从VolumeView中间接获取到MPVolumeSlider(改控件调节音量用的)
-(void)setUpVolumeViewSlider{
    _volumeView =[[MPVolumeView alloc]init];
   // [self addSubview:_volumeView];
    //寻找建立UISlider;
    // UISlider* volumeViewSlider = nil;
    //设置音量大小
    _volumeViewSlider.value = 0.5;
    for (UIView *view in [_volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            _volumeViewSlider = (UISlider*)view;
            _volumeViewSlider.backgroundColor = [UIColor yellowColor];
            break;
        }
    }
}
//播放状态代理
- (void)mediaPlayerStateChanged:(NSNotification *)aNotification{
    /**
     *  VLCMediaPlayerStateStopped,        //
     < 播放已经停止
     VLCMediaPlayerStateOpening,        //
     < 流是开发的
     VLCMediaPlayerStateBuffering,      //
     < 流正在缓冲
     VLCMediaPlayerStateEnded,          //
     < 流已经结束
     VLCMediaPlayerStateError,          //
     < 播放产生流错误
     VLCMediaPlayerStatePlaying,        //
     < Stream is playing
     VLCMediaPlayerStatePaused          //
     < Stream is paused
     */
    switch (_player.state) {
            //播放完或者手动停止
            
        case VLCMediaPlayerStateStopped:
        {
            [self videoStop];
            [self changePlayBtnState:    WasOver];
            //播放完后把开始播放标记设置为yes状态
            _startFlag=YES;
        }
            break;
            //播放中缓冲状态
        case VLCMediaPlayerStateBuffering:{
            [self changePlayBtnState:PlayBuffer];
        }
            break;
            //暂停状态被开始播放
        case VLCMediaPlayerStatePlaying:{
            [self changePlayBtnState:SuspendedToPlay];
        }
            break;
        case VLCMediaPlayerStatePaused:{
            [self changePlayBtnState:PlayToSuspended];
        }
            break;
        case VLCMediaPlayerStateError:{
            [self changePlayBtnState:PlayError];
        }
            break;
            
        case VLCMediaPlayerStateOpening:{
            [self changePlayBtnState:BufferOpen];
        }
            break;
    }
}
//当播放状态发生改变时
-(void)changePlayBtnState:(LWBPlayState )state{
    //播放按钮状态 yes播放状态 no暂停状态
    // 判断代理对象是否实现这个方法，没有实现会导致崩溃
        if ([self.delegate  respondsToSelector:@selector(changePlayBtnState:)]) {
            [self.delegate changePlayBtnState:state];
        }else{
            NSLog(@"changePlayBtnState:代理没实现");
            return;
        }
    
}
//当播放时间发生改变时
- (void)mediaPlayerTimeChanged:(NSNotification *)aNotification{
    //获取当前的播放进度
    int currentTime = self.player.time.intValue;
    if (_startFlag) {
        if ([self.delegate respondsToSelector:@selector(videoStartPlay)]) {
            [self.delegate videoStartPlay];
            //设置进度条最大进度
            _toolSlider.maximumValue =[self obtainVideoTotalTime];
        }else{
             NSLog(@"obtainVideoCurrentTime:代理没实现,但是它是可选的");
        }
    
        //该方法每次播放只执行一次，所以要设置为no
        _startFlag =NO;
    }
    if ([self.delegate respondsToSelector:@selector(obtainVideoCurrentTime:)]) {
        [self.delegate obtainVideoCurrentTime:currentTime];
        //设置进度条当前进度
        _toolSlider.value =currentTime;
    }else{
        NSLog(@"obtainVideoCurrentTime:代理没实现");
        return;
    }
      //NSLog(@"time:-->%d",currentTime);
}
//设置url
-(void)setURL:(NSString *)URL{
    _URL =URL;
    self.videoURL = [NSURL URLWithString:URL];
     VLCMedia *media =[VLCMedia mediaWithURL: _videoURL];
    self.player.media =media;
}
//设置本地播放路径
-(void)setPath:(NSString *)path{
    //NSLog(@"-->1%@",path);
    _path =path;
    VLCMedia *media =[VLCMedia mediaWithPath:path];
   // VLCTime *time =[VLCTime t]
    self.player.media = media;
}
//播放
-(void)videoPlay{
  //  NSLog(@"-->2%@",_path);
    if (_videoURL!=NULL||_path!=NULL) {
        _isPlay =YES;
        [self.player play];
         [_butPlay setImage:[UIImage imageNamed:@"Pause Icon"] forState:UIControlStateNormal];
    }else{
        ALERTVIEW(@"播放地址URL为空");
        return;

}
    
}
//暂停播放
-(void)videoPause{
        [_butPlay setImage:[UIImage imageNamed:@"Play Icon"] forState:UIControlStateNormal];
    _isPlay =NO;
    [self.player pause];
}
//停止播放
-(void)videoStop{
    _isPlay =NO;
    [self.player stop];
}

-(int)obtainVideoTotalTime{
    int AllTime = self.player.media.length.intValue;
    return AllTime;
}


//获取剩余时间
-(int)obtainVideoRemainingTime{
    int remainTime= self.player.remainingTime.intValue;
    return ABS(remainTime);
}
-(void)obtainHumbnail{
    //初始化并设置代理
    VLCMediaThumbnailer *thumbnailer = [VLCMediaThumbnailer thumbnailerWithMedia:self.player.media andDelegate:self];
    //    self.thumbnailer = thumbnailer;
    //开始获取缩略图
    [thumbnailer fetchThumbnail];
}
//获取缩略图超时
- (void)mediaThumbnailerDidTimeOut:(VLCMediaThumbnailer *)mediaThumbnailer{
    if ([self.delegate respondsToSelector:@selector(obtainHumbnailState:obtainImage:)]) {
        [self.delegate obtainHumbnailState:NO obtainImage:nil];
    }
//    NSLog(@"getThumbnailer time out.");
}
//获取缩略图成功
- (void)mediaThumbnailer:(VLCMediaThumbnailer *)mediaThumbnailer didFinishThumbnail:(CGImageRef)thumbnail{
    //获取缩略图
    UIImage *image = [UIImage imageWithCGImage:thumbnail];
    if ([self.delegate respondsToSelector:@selector(obtainHumbnailState:obtainImage:)]) {
        //BOOL state = NO;
        [self.delegate obtainHumbnailState:YES obtainImage:image];
    }
}
//设置播放速度
-(void)setUpPlaySpeed:(float)speed{
    self.player.rate = speed;
}
//设置当前从哪里开始播放（0.0-1.0  必须在视频开始播放后设置才能成功）
-(void)setUpCurrentProgress:(float)progress{
    [self.player setPosition:progress];
}
//获取当前播放位置
-(float)obtainCurrentProgress{
    return self.player.position;
}

//下载当前视频
-(NSString *)downloadCurrentVideoType:(NSString *)type{
   static NSString *pt=@"";
    if (_URL!=nil) {
        [self downloadVideoURL:_URL ofType:type Results:^(NSString * path, BOOL state) {
//            pt = path;
            if (state) {
                ALERTVIEW(@"下载成功");
                pt = path;
            }else{
            ALERTVIEW(@"下载失败");
            }
        }];
    }else{
        ALERTVIEW(@"当前路径url=nil");
    }
    return pt ;
}
-(void)downloadVideoURL:(NSString *)url ofType:(NSString *)type Results:(void (^)(NSString * path, BOOL state))results{
   // NSLog(@"%@;%@",url,type);
    NSURL *urlt =[NSURL URLWithString:url];
    //加数据装成data
    NSData *audioData = [NSData dataWithContentsOfURL:urlt];
    //将数据保存起来(获取Directories路径)
    NSString *docDirPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *currentTime =[self obtainCurrentTime];
    //设置一个文件名
    NSString *filePath = [NSString stringWithFormat:@"%@/Video/%@.%@",docDirPath,currentTime,type];
    //创建文件管理对象
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //获取需要判断的文件夹路径
    BOOL result = [fileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/Video",docDirPath]];
    //判断是否存在，不存在则创建
    if (!result) {
        BOOL fg =[self createDirectory:docDirPath andFileName:@"Video"];
        NSLog(@"保存的文件夹不存在,正再重新创建");
        if (fg) {
            NSLog(@"保存的文件夹创建成功");
        }else{
            NSLog(@"保存的文件夹创建失败");
        }
    }
    //保存写入
  BOOL stat =[audioData writeToFile:filePath atomically:YES];
    results(filePath,stat);
    NSLog(@"下载保存路径:%@",filePath);

}
//获取当前时间
-(NSString *)obtainCurrentTime{
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYYMMddhhmmss"];
    NSString *DateTime = [formatter stringFromDate:date];
    NSLog(@"%@============年-月-日  时：分：秒=====================",DateTime);
    return DateTime;
}
//读取已下载保存的视频
-(NSMutableArray *)readVideoFolder{
    NSMutableArray *videpath =[NSMutableArray new];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSLog(@"--->%@",docDir);
     NSFileManager *fileManage = [NSFileManager defaultManager];
    NSString *myDirectory = [docDir stringByAppendingPathComponent:@"Video"];
    NSArray *files = [fileManage subpathsOfDirectoryAtPath: myDirectory error:nil];
    for (int i=1; i<files.count; i++) {
        [videpath addObject:files[i]];
    }
     NSLog(@"%@",videpath);
     NSLog(@"目录--->%@", files);
    return videpath;
}
//创建目录
-(BOOL )createDirectory:(NSString *)path andFileName:(NSString *)name{
    //创建文件管理对象
     NSFileManager *fileManager = [NSFileManager defaultManager];
    //拼接要创建的文件夹路径
    NSString *testDirectory = [path stringByAppendingPathComponent:name];
    //创建文件夹
     BOOL results=[fileManager createDirectoryAtPath:testDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    return results;
}
//设置音量大小
-(void)setUpVolume:(float)value{
    _volumeViewSlider.value =value;
}
//获取音量大小
-(float)obtainVolume{
    return _volumeViewSlider.value;
}
-(void)setUpFullscreenPlay{
}
//设置屏幕亮度
-(void)setScreenBrightness:(float)value{
    [UIScreen mainScreen].brightness=value;
}
//获得屏幕亮度
-(float)obtainBrightness{
    return [UIScreen mainScreen].brightness;
}
//向前快进一段视频
-(void)forwardJumpingProgressVideo{
    [_player shortJumpForward];
}
//向前快进指定秒数视频
-(void)forwardJumpingSpecifiedTimeVideo:(int)time{
    [_player jumpForward:time];
}
//向后快进一段视频
-(void)backwardJumpingProgressVideo{
    [_player shortJumpBackward];
}

//向后快进指定秒数视频
-(void)backwardJumpingSpecifiedTimeVideo:(int)time{
    [_player jumpBackward:time];
}
-(void)setTapGestureRecognizer{
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap.numberOfTapsRequired = 1;//点击次数
   tap.numberOfTouchesRequired = 1;//手指数
    [self.gesturesView addGestureRecognizer:tap];
    UITapGestureRecognizer *tap1 =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
    tap1.numberOfTapsRequired =2;
    tap1.numberOfTouchesRequired=1;
    [self.gesturesView addGestureRecognizer:tap1];
    [tap requireGestureRecognizerToFail:tap1];
}

-(void)tapGesture:(UITapGestureRecognizer *)tap{
    if ([self.delegate respondsToSelector:@selector(tapGestureRecognizer:)]) {
        [self.delegate tapGestureRecognizer:tap];
    }
    if (!_isGiveupToolbar) {
        if (tap.numberOfTouchesRequired==1) {
            if (tap.numberOfTapsRequired==1) {
                
                if (_isToolbarHidden) {
                    [self hiddenToolbar];
                }else{
                    [self showToolbar];
                }
            }else if (tap.numberOfTapsRequired ==2){
                if (_isPlay) {
                    [self videoPause];
                }else{
                    [self videoPlay];
                }
            }
        }
    }
}
-(void)setUpPanGestureRecognizer{
    UIPanGestureRecognizer *panGest = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(dragDetectionAction:)];
       [self.gesturesView addGestureRecognizer:panGest];
}
-(void)dragDetectionAction:(UIPanGestureRecognizer *)pan{
     // UITouch *touch =pan.anyObject;
    CGPoint pote=[pan translationInView:pan.view];
    CGPoint point = [pan locationInView:self];
    // NSLog(@"坐标%f",point.x);
    if (pan.state==UIGestureRecognizerStateBegan) {
        if (point.x<self.frame.size.width/2) {
            //右侧上下滑动标记关闭 
            _panRightFlog =NO;
            _panLeftFlog =YES;
        }else{
             //左侧上下滑动标记关闭
            _panLeftFlog =NO;
            _panRightFlog =YES;
        }
         _currentVolume=[self obtainVolume];
        _currentBrightness = [self obtainBrightness];
        _currentProgress =0;
       // _gesturesStartCoordinates.origin = pote;
        //上下滑动和左右滑动标记关闭
        _gesturesFlogY=NO;
        _gesturesFlogX=NO;
    }else if (pan.state==UIGestureRecognizerStateChanged){
        //判断上下或者左右滑动是否有一个已经开启（开启说明已经判断成功用户想要执行的手势，所以不在进行下面的判断）
     //   NSLog(@"------>%f",pote.x);
        if (!(_gesturesFlogY==YES||_gesturesFlogX==YES)) {
            if (ABS(pote.x)>30) {
                _gesturesFlogY=NO;
                _gesturesFlogX=YES;
            }else if (ABS(pote.y)>30) {
            _gesturesFlogY=YES;
            _gesturesFlogX=NO;
                }

        }
        //判断左右手势标记是否打开，打开则执行下面代码
        if (_gesturesFlogY) {
            if (_panRightFlog) {
                //调节音量
                [self adjustVolume:_currentVolume andVolume:pote.y];
            }else if (_panLeftFlog){
                [self adjustBrightness:_currentBrightness andBrightness:pote.y];
            }
        }
        if (_gesturesFlogX) {
            [self adjustProgress:pote.x];
        }
        _currentProgress =pote.x;
    }
}
-(void)adjustVolume:(float)currentVolume andVolume:(float)volume{
    if (!(currentVolume==0||currentVolume==1)) {
        if (volume>0) {
            [self setUpVolume:currentVolume+volume/200];
        }else{
            [self setUpVolume:currentVolume-volume/200];
        }
    }
    NSLog(@"%f",[self obtainVolume]);
    
//NSLog(@"调节音量%f",volume);
    
}
-(void)adjustBrightness:(float)currentbrightness andBrightness:(float)brightness{
    if (!(currentbrightness==0||currentbrightness==1)) {
        if (brightness>0) {
            [self setUpVolume:currentbrightness+brightness/200];
        }else{
            [self setUpVolume:currentbrightness-brightness/200];
        }
    }
     //NSLog(@"调节亮度%f",brightness);
}
-(void)adjustProgress:(float)progress{
//    if (progress>0) {
//        [self forwardJumpingSpecifiedTimeVideo: progress-_currentProgress];
//    }else{
//        [self backwardJumpingSpecifiedTimeVideo:ABS(progress)]
//    }
    if (progress-_currentProgress>0) {
        [self forwardJumpingSpecifiedTimeVideo: progress-_currentProgress];
    }else{
        [self backwardJumpingSpecifiedTimeVideo:ABS(progress-_currentProgress)];
    }
    NSLog(@"调节进度");
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
