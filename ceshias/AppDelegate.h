//
//  AppDelegate.h
//  ceshias
//
//  Created by apple on 17/6/9.
//  Copyright © 2017年 LWB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

