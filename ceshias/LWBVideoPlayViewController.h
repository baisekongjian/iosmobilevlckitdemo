//
//  LWBVideoPlayViewController.h
//  ceshias
//
//  Created by apple on 17/6/9.
//  Copyright © 2017年 LWB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWBVideoPlayViewController : UIViewController
@property(nonatomic,strong)NSString *URL;
//视频盛放的view
-(void)videoPlay;
//暂停播放
-(void)videoPause;
//停止播放
-(void)videoStop;
-(void)setURL:(NSString *)URL;
-(void)setPlayView:(UIView *)playView;
@end
